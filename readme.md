# Places To Stay Flash

We use the [Sweet Alert](http://t4t5.github.io/sweetalert/) javascript library to add a nicer user experience when sending flash messages after taking an action on our site.

## Install

#### Composer

```json
{
  "require": {
    "placestostay/flash": "dev-master"
  },
  "repositories": [
    {"type": "vcs", "url": "https://placestostay@bitbucket.org/placestostay/flash.git"}
  ]
}
```

#### Service Provider

```php
<?php

return array(
    // typical providers above ...
    'providers' => Pts\Providers\FlashServiceProvider::class,

);
```


#### Publishing Configuration

```shell
$ php artisan vendor:publish --provider="Pts\Providers\FlashServiceProvider" --tag="config"
```


## Uninstall

more to follow


## Usage


```php
<?php

class Example
{
    public function action(\Illuminate\Http\Request $request, \Pts\Flash\Flasher $flash)
    {
        // Success 
        $message = $flash->success('It works!');
        $request->session()->flash('pts_flash', json_encode($message));
        
        return redirect()->back();
    }
}
```
    
#### Success

    $flash->success('Hi %s, you did something awesome! Nice job!', ['Cornelius']);

#### Warning

    $flash->warning('Something bad might happen! %s', ['Some var'], 'Title', 3);

#### Info

    $flash->info('Something happened!');

#### Error

    $flash->error('Oh noes! Tings blowed up!');
