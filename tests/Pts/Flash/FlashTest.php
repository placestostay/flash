<?php

class FlashTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Pts\Flash\Flasher */
    protected $unit;

    /** @var array */
    protected $config;

    public function setUp()
    {
        parent::setUp();

        $this->config = $this->getMock('Illuminate\Config\Repository', ['get'], [], '', false);
        $this->config
            ->expects($this->once())
            ->method('get')
            ->willReturn($this->getConfig());

        $this->unit = new \Pts\Flash\Flasher($this->config);
    }

    /** @test */
    public function it_can_create_a_basic_configuration_object_for_a_message()
    {
        $message = $this->unit->message('success', 'My message', []);
        $expected = [
            'type'  => 'success',
            'title' => 'Success',
            'text'  => 'My message',
            'timer' => null
        ];

        $this->assertEquals($expected['type'], $message['type']);
        $this->assertEquals($expected['title'], $message['title']);
        $this->assertEquals($expected['text'], $message['text']);
        $this->assertEquals($expected['timer'], $message['timer']);
    }

    /** @test */
    public function it_can_parse_variables_into_the_message()
    {
        $message = $this->unit->message('error', 'An error occurred: %s', ['Unable to eat own head']);
        $expected = [
            'type'  => 'error',
            'title' => 'Error',
            'text'  => 'An error occurred: Unable to eat own head',
            'timer' => null
        ];

        $this->assertEquals($expected['type'], $message['type']);
        $this->assertEquals($expected['title'], $message['title']);
        $this->assertEquals($expected['text'], $message['text']);
        $this->assertEquals($expected['timer'], $message['timer']);
    }

    /** @test */
    public function it_can_accept_a_custom_title()
    {
        $message = $this->unit->message('error', 'An error occurred', [], 'Oh Noes!');
        $expected = [
            'type'  => 'error',
            'title' => 'Oh Noes!',
            'text'  => 'An error occurred',
            'timer' => null
        ];

        $this->assertEquals($expected['type'], $message['type']);
        $this->assertEquals($expected['title'], $message['title']);
        $this->assertEquals($expected['timer'], $message['timer']);
        $this->assertEquals($expected['text'], $message['text']);
    }

    /** @test */
    public function it_sets_the_correct_number_of_seconds_when_timer_is_not_null()
    {
        $message = $this->unit->message('info', 'Message', [], null, 2);
        $expected = [
            'type'  => 'info',
            'title' => 'Info',
            'text'  => 'Message',
            'timer' => 2000
        ];

        $this->assertEquals($expected['type'], $message['type']);
        $this->assertEquals($expected['title'], $message['title']);
        $this->assertEquals($expected['timer'], $message['timer']);
        $this->assertEquals($expected['text'], $message['text']);
    }

    /** @test */
    public function it_can_quickly_create_error_messages()
    {
        $message = $this->unit->error('An error');
        $expected = [
            'type'  => 'error',
            'title' => 'Error',
            'text'  => 'An error'
        ];

        $this->assertEquals($expected['type'], $message['type']);
        $this->assertEquals($expected['title'], $message['title']);
        $this->assertEquals($expected['text'], $message['text']);
    }

    /** @test */
    public function it_can_quickly_create_warning_messages()
    {
        $message = $this->unit->warning('A warning');
        $expected = [
            'type'  => 'warning',
            'title' => 'Warning',
            'text'  => 'A warning'
        ];

        $this->assertEquals($expected['type'], $message['type']);
        $this->assertEquals($expected['title'], $message['title']);
        $this->assertEquals($expected['text'], $message['text']);
    }

    /** @test */
    public function it_can_quickly_create_info_messages()
    {
        $message = $this->unit->info('Info message');
        $expected = [
            'type'  => 'info',
            'title' => 'Info',
            'text'  => 'Info message'
        ];

        $this->assertEquals($expected['type'], $message['type']);
        $this->assertEquals($expected['title'], $message['title']);
        $this->assertEquals($expected['text'], $message['text']);
    }

    /** @test */
    public function it_can_quickly_create_success_messages()
    {
        $message = $this->unit->success('Successful message');
        $expected = [
            'type'  => 'success',
            'title' => 'Success',
            'text'  => 'Successful message'
        ];

        $this->assertEquals($expected['type'], $message['type']);
        $this->assertEquals($expected['title'], $message['title']);
        $this->assertEquals($expected['text'], $message['text']);
    }

    private function getConfig()
    {
        return include(realpath(__DIR__ . '/../../../src/config/pts_flash.php'));
    }
}
