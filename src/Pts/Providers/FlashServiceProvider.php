<?php

namespace Pts\Providers;

use Pts\Flash\Flasher;
use Illuminate\Support\ServiceProvider;

class FlashServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishConfig();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('\Pts\Flash\Flashable', function($app) {
            return new Flasher($app->make('config'));
        });
    }

    /**
     * Publish config files.
     */
    private function publishConfig()
    {
        $this->publishes([
            __DIR__.'/../../config/pts_flash.php' => config_path('pts_flash.php'),
        ], 'config');
    }
}
