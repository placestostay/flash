<?php
/**
 * Places To Stay Flash Alerts
 *
 * @package    Pts
 * @subpackage Pts\Flash
 * @category   Pts
 * @version    1.0
 */
namespace Pts\Flash;

/**
 * Places To Stay Flash Alerts Interface
 *
 * @package    Pts
 * @subpackage Pts\Flash
 */
interface Flashable
{
    /**
     * Create configuration for a flash message.
     *
     * @param  string    $status
     * @param  string    $message
     * @param  array     $vars
     * @param  string    $title
     * @param  null|int  $timer
     * @param  array     $options
     * @return string
     */
    public function message($status, $message, $vars = array(), $title = null, $timer = null, $options = []);

    /**
     * Create configuration for a success flash message.
     *
     * @param  string    $message
     * @param  array     $vars
     * @param  string    $title
     * @param  null|int  $timer
     * @param  array     $options
     * @return string
     */
    public function success($message, $vars = array(), $title = null, $timer = null, $options = []);

    /**
     * Create configuration for a error flash message.
     *
     * @param  string    $message
     * @param  array     $vars
     * @param  string    $title
     * @param  null|int  $timer
     * @param  array     $options
     * @return string
     */
    public function error($message, $vars = array(), $title = null, $timer = null, $options = []);

    /**
     * Create configuration for a success info message.
     *
     * @param  string    $message
     * @param  array     $vars
     * @param  string    $title
     * @param  null|int  $timer
     * @param  array     $options
     * @return string
     */
    public function info($message, $vars = array(), $title = null, $timer = null, $options = []);

    /**
     * Create configuration for a warning flash message.
     *
     * @param  string    $message
     * @param  array     $vars
     * @param  string    $title
     * @param  null|int  $timer
     * @param  array     $options
     * @return string
     */
    public function warning($message, $vars = array(), $title = null, $timer = null, $options = []);
}
