<?php

namespace Pts\Flash;

use Illuminate\Config\Repository;

class Flasher implements Flashable
{
    /** @var array */
    protected $options = [];

    /** @var Repository */
    protected $config;

    public function __construct(Repository $config)
    {
        $this->config = $config;
    }

    /**
     * Create configuration for a flash message.
     *
     * @param  string   $status
     * @param  string   $message
     * @param  array    $vars
     * @param  string   $title
     * @param  null|int $timer
     * @param  array    $options
     * @return string
     */
    public function message($status, $message, $vars = array(), $title = null, $timer = null, $options = [])
    {
        $defaults = array_merge($options, $this->config->get('pts_flash', []));
        $options  = array_merge($defaults, [
            'type'   => $status,
            'title'  => is_null($title) ? ucwords($status) : $title,
            'text'   => vsprintf($message, $vars),
            'timer'  => is_null($timer) ? null : (int) $timer * 1000
        ]);

        return $options;
    }

    /**
     * Create configuration for a success flash message.
     *
     * @param  string   $message
     * @param  array    $vars
     * @param  string   $title
     * @param  null|int $timer
     * @param  array    $options
     * @return string
     */
    public function success($message, $vars = array(), $title = null, $timer = null, $options = [])
    {
        return $this->message('success', $message, $vars, $title, $timer, $options);
    }

    /**
     * Create configuration for a error flash message.
     *
     * @param  string   $message
     * @param  array    $vars
     * @param  string   $title
     * @param  null|int $timer
     * @param  array    $options
     * @return string
     */
    public function error($message, $vars = array(), $title = null, $timer = null, $options = [])
    {
        return $this->message('error', $message, $vars, $title, $timer, $options);
    }

    /**
     * Create configuration for a success info message.
     *
     * @param  string   $message
     * @param  array    $vars
     * @param  string   $title
     * @param  null|int $timer
     * @param  array    $options
     * @return string
     */
    public function info($message, $vars = array(), $title = null, $timer = null, $options = [])
    {
        return $this->message('info', $message, $vars, $title, $timer, $options);
    }

    /**
     * Create configuration for a warning flash message.
     *
     * @param  string   $message
     * @param  array    $vars
     * @param  string   $title
     * @param  null|int $timer
     * @param  array    $options
     * @return string
     */
    public function warning($message, $vars = array(), $title = null, $timer = null, $options = [])
    {
        return $this->message('warning', $message, $vars, $title, $timer, $options);
}}