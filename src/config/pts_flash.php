<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title (required)
    |--------------------------------------------------------------------------
    |
    | The title of the modal. It can either be added to the object under the
    | key "title" or passed as the first parameter of the function.
    |
    */
    'title' => null,

    /*
    |--------------------------------------------------------------------------
    | Type
    |--------------------------------------------------------------------------
    |
    | The type of the modal. SweetAlert comes with 4 built-in types which will
    | show a corresponding icon animation: "warning", "error", "success" and
    | "info". You can also set it as "input" to get a prompt modal. It can
    | either be put in the object under the key "type" or passed as the
    | third parameter of the function.
    |
    */
    'type' => null,

    /*
    |--------------------------------------------------------------------------
    | Allow Escape Key
    |--------------------------------------------------------------------------
    | If set to true, the user can dismiss the modal by pressing the Escape key.
    |
    */
    'allowEscapeKey' => true,

    /*
    |--------------------------------------------------------------------------
    | customClass
    |--------------------------------------------------------------------------
    | A custom CSS class for the modal. It can be added to the object under the
    | key "customClass".
    |
    */
    'customClass' => null,

    /*
    |--------------------------------------------------------------------------
    | Allow Outside Click
    |--------------------------------------------------------------------------
    | If set to true, the user can dismiss the modal by clicking outside it.
    |
    */
    'allowOutsideClick' => false,

    /*
    |--------------------------------------------------------------------------
    | Show Cancel Button
    |--------------------------------------------------------------------------
    | If set to true, a "Cancel"-button will be shown, which the user can
    | click on to dismiss the modal.
    |
    */
    'showCancelButton' => false,

    /*
    |--------------------------------------------------------------------------
    | Show Confirm Button
    |--------------------------------------------------------------------------
    | If set to false, the "OK/Confirm"-button will be hidden. Make sure you
    | set a timer or set allowOutsideClick to true when using this, in
    | order not to annoy the user.
    |
    */
    'showConfirmButton' => false,

    /*
    |--------------------------------------------------------------------------
    | Confirm Button Text
    |--------------------------------------------------------------------------
    | Use this to change the text on the "Confirm"-button. If showCancelButton
    | is set as true, the confirm button will automatically show "Confirm"
    | instead of "OK".
    |
    */
    'confirmButtonText' => "OK",

    /*
    |--------------------------------------------------------------------------
    | Confirm Button Color
    |--------------------------------------------------------------------------
    | Use this to change the background color of the "Confirm"-button (must
    | be a HEX value).
    |
    */
    'confirmButtonColor' => "#AEDEF4",

    /*
    |--------------------------------------------------------------------------
    | Cancel Button Text
    |--------------------------------------------------------------------------
    | Use this to change the text on the "Cancel"-button.
    |
    */
    'cancelButtonText' => "Cancel",

    /*
    |--------------------------------------------------------------------------
    | Close On Confirm
    |--------------------------------------------------------------------------
    | Set to false if you want the modal to stay open even if the user presses
    | the "Confirm"-button. This is especially useful if the function attached
    | to the "Confirm"-button is another SweetAlert.
    |
    */
    'closeOnConfirm' => true,

    /*
    |--------------------------------------------------------------------------
    | Close On Cancel
    |--------------------------------------------------------------------------
    | Same as closeOnConfirm, but for the cancel button.
    |
    */
    'closeOnCancel'	=> true,

    /*
    |--------------------------------------------------------------------------
    | Image Url
    |--------------------------------------------------------------------------
    | Add a customized icon for the modal. Should contain a string with the
    | path to the image.
    |
    */
    'imageUrl' => null,

    /*
    |--------------------------------------------------------------------------
    | Image Size
    |--------------------------------------------------------------------------
    | If imageUrl is set, you can specify imageSize to describes how big you
    | want the icon to be in px. Pass in a string with two values separated
    | by an "x". The first value is the width, the second is the height.
    |
    */
    'imageSize' => "80x80",

    /*
    |--------------------------------------------------------------------------
    | Timer
    |--------------------------------------------------------------------------
    | Auto close timer of the modal. Set in ms (milliseconds).
    |
    */
    'timer' => null,

    /*
    |--------------------------------------------------------------------------
    | Html
    |--------------------------------------------------------------------------
    | If set to true, will not escape title and text parameters. (Set to false
    |  if you're worried about XSS attacks.)
    |
    */
    'html' => false,

    /*
    |--------------------------------------------------------------------------
    | animation
    |--------------------------------------------------------------------------
    | If set to false, the modal's animation will be disabled. Possible
    | (string) values : pop (default when animation set to true),
    | slide-from-top, slide-from-bottom
    |
    */
    'animation' => true,

    /*
    |--------------------------------------------------------------------------
    | Input Type
    |--------------------------------------------------------------------------
    | Change the type of the input field when using type: "input" (this can
    | be useful if you want users to type in their password for example).
    |
    */
    'inputType' => "text",

    /*
    |--------------------------------------------------------------------------
    | Input Placeholder
    |--------------------------------------------------------------------------
    | When using the input-type, you can specify a placeholder to help the user.
    |
    */
    'inputPlaceholder' => null,

    /*
    |--------------------------------------------------------------------------
    | Input Value
    |--------------------------------------------------------------------------
    | Specify a default text value that you want your input to show when
    | using type: "input"
    |
    */
    'inputValue' => null,

    /*
    |--------------------------------------------------------------------------
    | Show Loader On Confirm
    |--------------------------------------------------------------------------
    | Set to true to disable the buttons and show that something is loading.
    |
    */
    'showLoaderOnConfirm' => false

];
